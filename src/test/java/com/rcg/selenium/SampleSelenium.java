package com.rcg.selenium;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SampleSelenium {
	WebDriver driver;

	@BeforeClass
	public static void setupDriver() throws Exception {

		// if (browser == "firefox") {
		// System.setProperty("webdriver.firefox.driver", "geckodriver.exe");
		// // driver = new FirefoxDriver();
		// } else if (browser == "chrome") {
		// // System.setProperty("webdriver.chrome.driver",
		// // "chromedriver.exe");
		// // driver = new ChromeDriver();
		// ChromeDriverManager.getInstance().setup();
		// } else if (browser == "iexplorer") {
		// System.setProperty("webdriver.ie.driver",
		// "IEDriverServer_Win32.exe");
		// DesiredCapabilities ieCapabilities = DesiredCapabilities
		// .internetExplorer();
		// ieCapabilities
		// .setCapability(
		// InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
		// true);
		// // driver = new InternetExplorerDriver();
		// // default browser indicate here
		// } else {
		// // System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		// // driver = new ChromeDriver();
		// }

		// return driver;

	}

	public WebDriver setupWebDriver(String browser)  {
		if (browser == "firefox") {
//			 System.setProperty("webdriver.firefox.driver",
//			 "geckodriver.exe");
//			 driver = new FirefoxDriver();
			FirefoxDriverManager.getInstance().setup();
			DesiredCapabilities capabilities=DesiredCapabilities.firefox();
			System.out.println("Debug 2");
			capabilities.setCapability("marionette", true);
			System.out.println("Debug 3");
			driver = new FirefoxDriver();
			System.out.println("Debug 4");
		} else if (browser == "chrome") {
			// System.setProperty("webdriver.chrome.driver",
			// "chromedriver.exe");
			ChromeDriverManager.getInstance().setup();
			driver = new ChromeDriver();
			// ChromeDriverManager.getInstance().setup();
		} else if (browser == "iexplorer") {
			// System.setProperty("webdriver.ie.driver",
			// "IEDriverServer_Win32.exe");
			InternetExplorerDriverManager.getInstance().setup();
			DesiredCapabilities ieCapabilities = DesiredCapabilities
					.internetExplorer();
			ieCapabilities
					.setCapability(
							InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);
			driver = new InternetExplorerDriver();
			// default browser indicate here
		} else {
			InternetExplorerDriverManager.getInstance().setup();
			DesiredCapabilities ieCapabilities = DesiredCapabilities
					.internetExplorer();
			ieCapabilities
					.setCapability(
							InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);
			driver = new InternetExplorerDriver();
		}

		return driver;

	}

	@Before
	public void setupTest() {
		// insert some codes here
		driver = setupWebDriver("firefox");
	}

	@Test
	public void FirstTestCase() {

		String baseUrl = "https://en.wikipedia.org/wiki/Main_Page";
		String expectedTitle = "Wikipedia, the free encyclopedia";
		String actualTitle = "";

		// launch Fire fox and direct it to the Base URL
		driver.get(baseUrl);

		// get the actual value of the title
		actualTitle = driver.getTitle();

		/*
		 * compare the actual title of the page with the expected one and print
		 * the result as "Passed" or "Failed"
		 */
		if (actualTitle.contentEquals(expectedTitle)) {
			System.out.println("Test Passed!");
		} else {
			System.out.println("Test Failed");
		}

		// close Fire fox
		driver.close();
	}

	@Test
	public void SecondTestCase() {
		// WebDriver driver = setupDriver("");

		String baseUrl = "https://en.wikipedia.org/wiki/Main_Page";

		driver.get(baseUrl);
		WebElement articleCount = driver.findElement(By.id("articlecount"));
		System.out.println("Total article as of today : "
				+ articleCount.getText());

		driver.findElement(By.cssSelector("a[href='/wiki/Portal:Contents']"))
				.click();

		driver.close();

	}

}
