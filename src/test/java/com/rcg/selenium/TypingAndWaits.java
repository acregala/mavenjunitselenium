package com.rcg.selenium;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TypingAndWaits {

	WebDriver driver;
	
	@Before
	public void setup(){
//		driver = new FirefoxDriver();
		System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
		driver = new InternetExplorerDriver();
//		driver = new FirefoxDriver();
		driver.get("https://en.wikipedia.org/wiki/Main_Page");
	}
	
	@Ignore
	@Test
	public void test() throws InterruptedException{
		
		
		WebElement search = driver.findElement(By.id("searchInput"));
		
		search.sendKeys("Test");
		search.sendKeys("qwertt");	//appended
		
		
		TimeUnit.SECONDS.sleep(5);
	}
	
	@Ignore
	@Test
	public void selects() throws InterruptedException{
		
		WebElement sel = driver.findElement(By.id("dropdown_7"));
		
		Select languageDropdown = new Select(sel);

//		languageDropdown.selectByVisibleText("Algeria");
//		languageDropdown.selectByIndex(6);
//		languageDropdown.selectByValue("en");
		
		
		Select dateDay = new Select(driver.findElement(By.id("dd_date_8")));
		dateDay.selectByValue("3");
		
		TimeUnit.SECONDS.sleep(2);
		
		dateDay.selectByValue("");
		
		
		TimeUnit.SECONDS.sleep(5);
	}

	@Ignore
	@Test
	public void driverFunctions() throws InterruptedException{
//		System.out.println(driver.getTitle());
//		System.out.println(driver.getPageSource());
//		System.out.println(driver.getCurrentUrl());
		
		driver.navigate().to("http://www.google.com");
		
		TimeUnit.SECONDS.sleep(5);
		driver.navigate().refresh();
		
		TimeUnit.SECONDS.sleep(5);
		driver.navigate().back();
		
		TimeUnit.SECONDS.sleep(5);
		driver.navigate().forward();
	}
	
	@Test
	public void waits() throws InterruptedException{

		WebElement search = driver.findElement(By.id("searchInput"));
        WebDriverWait myWaitVar = new WebDriverWait(driver, 10);

        myWaitVar.until(ExpectedConditions.visibilityOf(search));

		search.sendKeys("Test");
		search.sendKeys("qwertt");	//appended

	}
	
	@After
	public void cleanup(){
		driver.quit();
	}

}
